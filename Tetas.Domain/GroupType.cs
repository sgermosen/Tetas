﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Tetas.Domain
{
    public class GroupType
    {
        [Key]
        public int GroupTypeId { get; set; }

        public string Name { get; set; }

        [JsonIgnore]
        public virtual  ICollection<Group> Groups { get; set; }

    }
}
