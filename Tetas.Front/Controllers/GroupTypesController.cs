﻿ using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tetas.Domain;
using Tetas.Front.Models;

namespace Tetas.Front.Controllers
{
    public class GroupTypesController : Controller
    {
        private LocalDataContext db = new LocalDataContext();

        // GET: GroupTypes
        public async Task<ActionResult> Index()
        {
            return View(await db.GroupTypes.ToListAsync());
        }

        // GET: GroupTypes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GroupType groupType = await db.GroupTypes.FindAsync(id);
            if (groupType == null)
            {
                return HttpNotFound();
            }
            return View(groupType);
        }

        // GET: GroupTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GroupTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "GroupTypeId,Name")] GroupType groupType)
        {
            if (ModelState.IsValid)
            {
                db.GroupTypes.Add(groupType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(groupType);
        }

        // GET: GroupTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GroupType groupType = await db.GroupTypes.FindAsync(id);
            if (groupType == null)
            {
                return HttpNotFound();
            }
            return View(groupType);
        }

        // POST: GroupTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "GroupTypeId,Name")] GroupType groupType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(groupType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(groupType);
        }

        // GET: GroupTypes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GroupType groupType = await db.GroupTypes.FindAsync(id);
            if (groupType == null)
            {
                return HttpNotFound();
            }
            return View(groupType);
        }

        // POST: GroupTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            GroupType groupType = await db.GroupTypes.FindAsync(id);
            db.GroupTypes.Remove(groupType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
