﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tetas.Domain;
using Tetas.Front.Models;

namespace Tetas.Front.Controllers
{
    public class PrivaciesController : Controller
    {
        private LocalDataContext db = new LocalDataContext();

        // GET: Privacies
        public async Task<ActionResult> Index()
        {
            return View(await db.Privacies.ToListAsync());
        }

        // GET: Privacies/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Privacy privacy = await db.Privacies.FindAsync(id);
            if (privacy == null)
            {
                return HttpNotFound();
            }
            return View(privacy);
        }

        // GET: Privacies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Privacies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "PrivacyId,Name")] Privacy privacy)
        {
            if (ModelState.IsValid)
            {
                db.Privacies.Add(privacy);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(privacy);
        }

        // GET: Privacies/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Privacy privacy = await db.Privacies.FindAsync(id);
            if (privacy == null)
            {
                return HttpNotFound();
            }
            return View(privacy);
        }

        // POST: Privacies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "PrivacyId,Name")] Privacy privacy)
        {
            if (ModelState.IsValid)
            {
                db.Entry(privacy).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(privacy);
        }

        // GET: Privacies/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Privacy privacy = await db.Privacies.FindAsync(id);
            if (privacy == null)
            {
                return HttpNotFound();
            }
            return View(privacy);
        }

        // POST: Privacies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Privacy privacy = await db.Privacies.FindAsync(id);
            db.Privacies.Remove(privacy);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
