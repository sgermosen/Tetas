﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Tetas.Domain;
using Tetas.Front.Helpers;
using Tetas.Front.Models;

namespace Tetas.Front.Controllers
{
    public class UserPostsController : Controller
    {
        private LocalDataContext db = new LocalDataContext();

        public async Task<int> GetUserId()
        {
            //if (Session["UserId"] != null && Convert.ToInt32(Session["UserId"]) != 0) return Convert.ToInt32(Session["UserId"]);
            var manager =
                new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var currentUser = manager.FindById(User.Identity.GetUserId());
            // Session["UserId"] =
            return await UsersHelper.GetUserId(currentUser.Email);
            //return Convert.ToInt32(Session["UserId"]);
        }

        #region Comments
        public async Task<ActionResult> CreateComment(int id)
        {
            var userId = await GetUserId();

            var userPost = await db.UserPosts.FindAsync(id);
            if (userPost == null)
            {
                return HttpNotFound();
            }
            var comment = new PostComment
            {
                UserPostId = id,
                UserId = userId
            };

            return View(comment);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateComment(PostComment comment)
        {
            if (ModelState.IsValid)
            {
                comment.CommentDate = DateTime.Now.ToUniversalTime();
                db.PostComments.Add(comment);
                await db.SaveChangesAsync();
                return RedirectToAction("Index", "Home");
            }
            return View(comment);
        }

        public async Task<ActionResult> EditComment(int id)
        {
            var userId = await GetUserId();

            var comment = await db.PostComments.FindAsync(id);
            if (comment == null)
            {
                return View("Error");
            }
            //supposily the user than is editing is the logged user, but let's validate 
            return comment.UserId != userId ? View("Error") : View(comment);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditComment(PostComment comment)
        {
            if (!ModelState.IsValid) return View(comment);
            comment.ModifiedDate = DateTime.Now.ToUniversalTime();
            db.Entry(comment).State=EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> DeleteComment(int id)
        {
            //  var userPost = await db.UserPosts.FindAsync( id);

            var comment = await db.PostComments.FindAsync(id);
            if (comment == null)
            {
                return View("Error");

            }
            
            db.PostComments.Remove(comment);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return View("Error");
            }

            return RedirectToAction("Index", "Home");


        }
        #endregion

        #region Post




        public async Task<ActionResult> Index()
        {
            var userPosts = db.UserPosts.Include(u => u.User);

            return View(await userPosts.ToListAsync());
        }

        // GET: UserPosts/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserPost userPost = await db.UserPosts.FindAsync(id);
            if (userPost == null)
            {
                return HttpNotFound();
            }
            return View(userPost);
        }


        public async Task<ActionResult> Create()
        {
            var userId = await GetUserId();
            var userPost = new UserPost { UserId = userId };
            return View(userPost);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(UserPost userPost)
        {
            if (ModelState.IsValid)
            {
                userPost.PostDate = DateTime.Now.ToUniversalTime();
                db.UserPosts.Add(userPost);
                await db.SaveChangesAsync();
                return RedirectToAction("Index", "Home");
            }

            ViewBag.UserId = new SelectList(db.Users, "UserId", "Email", userPost.UserId);
            return View(userPost);
        }

        // GET: UserPosts/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var userId = await GetUserId();
            var userPost = await db.UserPosts.FindAsync(id);
            if (userPost == null)
            {
                return HttpNotFound();
            }

            userPost.UserId = userId;

             return View(userPost);
        }

        // POST: UserPosts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(UserPost userPost)
        {
            if (!ModelState.IsValid) return View(userPost);
            db.Entry(userPost).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id)
        {
          //  var userPost = await db.UserPosts.FindAsync( id);

          var userPost = await  db.UserPosts.Include(p=>p.PostComments).FirstOrDefaultAsync(p=>p.UserPostId==id);
            if (userPost == null)
            {
                return View("Error");

            }

            foreach (var comment in userPost.PostComments.ToList())
            {
                try
                {
                    //if (userPost.PostComments.All(c => c.PostCommentId != comment.PostCommentId))
                       // db.PostComments.Remove(comment);
                 var commToDelete =  await db.PostComments.FindAsync(comment.PostCommentId);

                     if (commToDelete != null) db.PostComments.Remove(commToDelete);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
               
            }
            try
            {
                await db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return View("Error");
            }

            db.UserPosts.Remove(userPost);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return View("Error");
            }

            return RedirectToAction("Index", "Home");
           
          
        }


        #endregion
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
