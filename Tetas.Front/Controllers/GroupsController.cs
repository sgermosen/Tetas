﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Tetas.Domain;
using Tetas.Front.Helpers;
using Tetas.Front.Models;
using Tetas.Tools;

namespace Tetas.Front.Controllers
{
    public class GroupsController : Controller
    {
        private LocalDataContext db = new LocalDataContext();

        // GET: Groups
        public async Task<ActionResult> Index(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var groups = db.Groups.Include(g => g.GroupType).Include(g => g.User)
                .Where(p => p.UserId == (int)id);
            return View(await groups.ToListAsync());
        }

        public async Task<int> GetUserId()
        {
            //if (Session["UserId"] != null && Convert.ToInt32(Session["UserId"]) != 0) return Convert.ToInt32(Session["UserId"]);
            var manager =
                new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var currentUser = manager.FindById(User.Identity.GetUserId());
            if (currentUser != null)
            {
                return await UsersHelper.GetUserId(currentUser.Email);
            }
            return 0;
            // Session["UserId"] =

            //return Convert.ToInt32(Session["UserId"]);
        }
        public async Task<ActionResult> IndexRequest(int id)
        {
            var userId = await GetUserId();
            ViewBag.ConectedUserId = userId;
            var member = db.GroupMembers.Where( p=>p.GroupId==id)
                .Include(g => g.User)
                .Include(u => u.Group);

            return View(await member.ToListAsync());
        }

        public async Task<ActionResult> IndexAll()
        {
            var userId = await GetUserId();
            ViewBag.ConectedUserId = userId;
            var groups = db.Groups.Include(g => g.GroupType)
                .Include(g => g.User)
                .Include(u => u.GroupMembers);
            return View(await groups.ToListAsync());
        }
        public async Task<ActionResult> Apply(int id)
        {
            var userId = await GetUserId();

            var group = db.Groups.FindAsync(id);

            if (group == null)
            {
                return View("Error");
            }

            var member = new GroupMember
            {
                UserId = userId,
                StatusId = 6,
                GroupId = id,
                ApplicationDate = DateTime.Now.ToUniversalTime()
            };

            db.GroupMembers.Add(member);

            await db.SaveChangesAsync();

            return RedirectToAction("IndexAll");
        }

        public async Task<ActionResult> ManageUsers(int id, int statusid, int groupid)
        {
            var member = await db.GroupMembers.FirstOrDefaultAsync(p => p.UserId == id && p.GroupId == groupid);

            if (member == null)
            {
                return HttpNotFound();
            }

            member.StatusId = statusid;

            return RedirectToAction($"Details/{groupid}");
    }
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var userId = await GetUserId();
            var group =  db.Groups.Find(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            ViewBag.ConectedUserId = userId;
            return View(group);
        }

        // GET: Groups/Create
        public async Task<ActionResult> Create(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.GroupTypeId = new SelectList(db.GroupTypes, "GroupTypeId", "Name");
            ViewBag.PrivacyId = new SelectList(db.Privacies, "PrivacyId", "Name");

            var newGroup = new GroupView { UserId = (int)id };
            return View(newGroup);
        }

        // POST: Groups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(GroupView view)
        {
            if (ModelState.IsValid)
            {
                var pic = string.Empty;
                const string folder = "~/Content/GroupPics";

                if (view.ImageFile != null)
                {
                    pic = Files.UploadPhoto(view.ImageFile, folder, "");
                    pic = string.Format("{0}/{1}", folder, pic);
                }
                var group = new Group
                {
                    CreationDate = DateTime.Today.ToUniversalTime(),
                    GroupId = view.GroupId,
                    UserId = view.UserId,
                    GroupTypeId = view.GroupTypeId,
                    Link = view.Link,
                    Name = view.Name
                };

                group.Picture = pic;

                db.Groups.Add(group);
                await db.SaveChangesAsync();

                var member = new GroupMember
                {
                    UserId = view.UserId,
                    GroupId = group.GroupId,
                    ApplicationDate = DateTime.Today.ToUniversalTime(),
                    StatusId = 1
                };

                db.GroupMembers.Add(member);

                await db.SaveChangesAsync();

                return RedirectToAction($"Index/{view.UserId}");
            }

            ViewBag.GroupTypeId = new SelectList(db.GroupTypes, "GroupTypeId", "Name", view.GroupTypeId);
            return View(view);
        }

        // GET: Groups/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = await db.Groups.FindAsync(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            ViewBag.GroupTypeId = new SelectList(db.GroupTypes, "GroupTypeId", "Name", group.GroupTypeId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "Email", group.UserId);
            return View(group);
        }

        // POST: Groups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "GroupId,GroupTypeId,UserId,Name,Link,Picture,CreationDate")] Group group)
        {
            if (ModelState.IsValid)
            {
                db.Entry(group).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.GroupTypeId = new SelectList(db.GroupTypes, "GroupTypeId", "Name", group.GroupTypeId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "Email", group.UserId);
            return View(group);
        }

        // GET: Groups/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = await db.Groups.FindAsync(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        // POST: Groups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Group group = await db.Groups.FindAsync(id);
            db.Groups.Remove(group);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
