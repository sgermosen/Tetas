﻿using System.IO;

namespace Tetas.Front.Controllers
{
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Helpers;
    using Models;
    using System.Web.Mvc;

    public class HomeController : Controller
    {
        private LocalDataContext db = new LocalDataContext();
        public async Task<int> GetUserId()
        {
            //if (Session["UserId"] != null && Convert.ToInt32(Session["UserId"]) != 0) return Convert.ToInt32(Session["UserId"]);
            var manager =
                new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var currentUser = manager.FindById(User.Identity.GetUserId());
            if (currentUser != null)
            {
                return await UsersHelper.GetUserId(currentUser.Email);
            }
            return 0;
            // Session["UserId"] =

            //return Convert.ToInt32(Session["UserId"]);
        }

        public async Task<ActionResult> Index()
        {

            var userId = await GetUserId();
            if (userId == 0)
            {
                return RedirectToAction("Login","Account");
            }
            var userPosts = db.UserPosts.Include(u => u.User)
                .Include(p => p.PostComments).OrderByDescending((p => p.PostDate));

            ViewBag.ConectedUserId = userId;

            //loading groups
            var userGroups = await db.Groups.Include(u => u.User)
                .OrderByDescending(p => p.Name).Where(p => p.UserId == userId).ToListAsync();
            ViewBag.UserGroups = userGroups;
           //  RenderRazorViewToString("_GroupsListPartial",  userGroups);

            return View(await userPosts.ToListAsync());
        }
        protected string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                    viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                    ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        public async Task<ActionResult> GroupList(int? id)
        {
            var userId = 0;
            if (id == null)
            {
                userId = await GetUserId();
            }
            else
            {
                userId = (int) id;
            }

             
            var userGroups = db.Groups.Include(u => u.User)
                .OrderByDescending(p => p.Name).Where(p=>p.UserId==userId);
           
            return View(await userGroups.ToListAsync());
        }

        public ActionResult About()
        {
            ViewBag.Message = "";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}