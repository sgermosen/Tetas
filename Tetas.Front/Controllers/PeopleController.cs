﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Tetas.Domain;
using Tetas.Front.Models;
using Tetas.Tools;

namespace Tetas.Front.Controllers
{
   
    public class PeopleController : Controller
    {
        private LocalDataContext db = new LocalDataContext();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

         

        public ApplicationUserManager UserManager
        {
            get {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set {
                _userManager = value;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        public ApplicationSignInManager SignInManager
        {
            get {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set {
                _signInManager = value;
            }
        }
        [Authorize]
        public async Task<ActionResult> Index()
        {
            var people = db.Users.Include(p => p.Country).Include(p => p.Gender);

            return View(await people.ToListAsync());
        }

        [Authorize]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var person = await db.Users.FindAsync(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

         
        public ActionResult Create()
        {
            ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "Name");
            ViewBag.GenderId = new SelectList(db.Genders, "GenderId", "Name");
            ViewBag.StatusId = new SelectList(db.Status, "StatusId", "Name");
            ViewBag.UserTypeId = new SelectList(db.UserTypes, "UserTypeId", "Name");

            return View();
        }

         
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(UserView userView)
        {
            if (ModelState.IsValid)
            {
                var pic = string.Empty;
                const string folder = "~/Content/UserPics";

                if (userView.ImageFile != null)
                {
                    pic = Files.UploadPhoto(userView.ImageFile, folder, "");
                    pic = string.Format("{0}/{1}", folder, pic);
                }

              
               

                var userAsp = new ApplicationUser { UserName = userView.Email, Email = userView.Email };
                var result = await UserManager.CreateAsync(userAsp, userView.Password);
             
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(userAsp, isPersistent: false, rememberBrowser: false);

                    var user = new User
                    {
                        Email = userView.Email,
                        StatusId = userView.StatusId,
                        UserTypeId = userView.UserTypeId
                  ,Name = userView.Name,
                        LastName = userView.LastName,
                        BornDate = userView.BornDate,
                        CountryId = userView.CountryId,
                        GenderId = userView.GenderId
                    };

user.Picture = pic;

                    db.Users.Add(user);
                    await db.SaveChangesAsync();

                    return RedirectToAction("Index", "Home");
                }
                 AddErrors(result);
               
               // return RedirectToAction("Index");
            }

            ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "Name", userView.CountryId);
            ViewBag.GenderId = new SelectList(db.Genders, "GenderId", "Name", userView.GenderId);
            ViewBag.StatusId = new SelectList(db.Status, "StatusId", "Name", userView.GenderId);
            ViewBag.UserTypeId = new SelectList(db.UserTypes, "UserTypeId", "Name", userView.GenderId);
          
            return View(userView);

        }

        [Authorize]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var person = await db.Users.FindAsync(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "Name", person.CountryId);
            ViewBag.GenderId = new SelectList(db.Genders, "GenderId", "Name", person.GenderId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "Email", person.UserId);
            return View(person);
        }

    
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(User person)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CountryId = new SelectList(db.Countries, "CountryId", "Name", person.CountryId);
            ViewBag.GenderId = new SelectList(db.Genders, "GenderId", "Name", person.GenderId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "Email", person.UserId);
            return View(person);
        }

        [Authorize]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var person = await db.Users.FindAsync(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

      
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var person = await db.Users.FindAsync(id);
            db.Users.Remove(person);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
