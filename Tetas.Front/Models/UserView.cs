﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Tetas.Front.Models
{
    using Domain;
    using System.ComponentModel.DataAnnotations.Schema;

    [NotMapped]
    public class UserView : User
    {
       
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name="Imagen")]
        public HttpPostedFileBase ImageFile { get; set; }

       
        

    }
}